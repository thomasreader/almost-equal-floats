package com.gitlab.thomasreader.almostequalfloats

import kotlin.math.absoluteValue
import kotlin.math.max
import kotlin.math.sign

/**
 * The difference between 1.0 and the smallest double greater than 1.0 (```2^0 * (1 + 1 / 2^52) - 1```)
 */
inline val Double.Companion.DBL_EPSILON: Double
    get() = 2.2204460492503131E-16

/**
 * Delegated compound comparison operation combining equality, epsilon equality and ulp equality to determine whether
 * this and the specified double are almost equal. This provides overlapping coverage where epsilon comparisons between
 * doubles can provide better equality than ulp which struggles comparing positive and negative numbers.
 *
 * **See:** [RandomASCII - Comparing Floating Point Numbers, 2012 Edition](https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/)
 *
 * @param that the comparison double
 * @param ulp the allowed difference in units in last place
 * @param epsilon the epsilon
 * @return true if this==that or either of the delegate operations return true, false otherwise
 * @see Double.equalsEpsilon
 * @see Double.equalsUlp
 */
fun Double.equalsEpsilonUlp(that: Double, ulp: Long = 1, epsilon: Double = Double.DBL_EPSILON): Boolean {
    return this == that || this.equalsEpsilon(that, epsilon) || this.equalsUlp(that, ulp)
}

/**
 * Delegated compound comparison operation combining equality, epsilon equality and relative equality to determine
 * whether this and the specified double are almost equal. This provides overlapping coverage where epsilon comparisons
 * between doubles can provide better equality than relative difference with small floats around 0.
 *
 * **See:** [RandomASCII - Comparing Floating Point Numbers, 2012 Edition](https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/)
 *
 * @param that the comparison double
 * @param relDiff the relative difference
 * @param epsilon the epsilon
 * @return true if this==that or either of the delegate operations return true, false otherwise
 * @see Float.equalsEpsilon
 * @see Float.relativeDifference
 */
fun Double.equalsEpsilonRelDiff(that: Double, relDiff: Double = Double.DBL_EPSILON, epsilon: Double = Double.DBL_EPSILON): Boolean {
    return this == that || this.equalsEpsilon(that, epsilon) || this.equalsRelDifference(that, relDiff)
}

/**
 * Compares this value with the specified double to determine whether they are almost equal by seeing if the difference
 * is less than or equal to the epsilon.
 *
 * @param that the comparison double
 * @param epsilon the machine epsilon
 * @return true if the absolute difference <= epsilon
 */
fun Double.equalsEpsilon(that: Double, epsilon: Double = Double.DBL_EPSILON): Boolean {
    return (this - that).absoluteValue <= epsilon.absoluteValue
}

/**
 * Compares this value with the specified double to see whether they are almost equal in reference to the specified
 * relative difference ratio. This algorithm compares the absolute difference in doubles to the provided allowed ratio;
 * it doesn't work as well with very small numbers.
 *
 * @param that the comparison double
 * @param relDiff the allowed difference ratio
 * @return true if this.relativeDiff(that) <= relDiff
 * @see Double.relativeDifference
 */
fun Double.equalsRelDifference(that: Double, relDiff: Double = Double.DBL_EPSILON): Boolean {
    return this.relativeDifference(that) <= relDiff
}

/**
 * Calculates the relative difference between two doubles.
 *
 * **See:** [C-FAQ - FAQ list Question 14.5 ](http://c-faq.com/fp/fpequal.html)
 *
 * @param that the comparison double
 * @return 0.0 if the two doubles are equal, 1.0 if one of the doubles equals 0.0, else a ratio > 0.0
 */
infix fun Double.relativeDifference(that: Double): Double {
    if (this == that) {
        return 0.0
    }
    val absDiff = (this - that).absoluteValue
    val largestVal = max(this.absoluteValue, that.absoluteValue)
    return absDiff / largestVal
}

/**
 * Compares this value with the specified double to see whether they are almost equal in reference to the specified
 * ulp long integer. This algorithm compares the actual difference in ulp to the provided allowed difference and works
 * best with numbers of the same sign.
 *
 * @param that the comparison double
 * @param ulp allowed units in the last place difference
 * @return true if this.ulp(that) <= ulp, false otherwise
 * @see Double.ulp
 */
fun Double.equalsUlp(that: Double, ulp: Long = 1): Boolean {
    if (this == that) {
        return true
    }
    if (this.sign != that.sign) {
        return false
    }
    return this.ulp(that).absoluteValue <= ulp
}

/**
 * Calculates the unit in the last place (ulp) difference between long integer representations of two doubles. The
 * returned value may overflow if the numbers are far enough apart.
 *
 * **See:** [RandomASCII - Comparing Floating Point Numbers, 2012 Edition](https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/)
 * **See:** [Wikipedia - Unit in the last place](https://en.wikipedia.org/wiki/Unit_in_the_last_place)
 *
 * @param that the comparison double
 * @return the ulp which may overflow
 */
infix fun Double.ulp(that: Double): Long {
    return this.toRawBits() - that.toRawBits()
}