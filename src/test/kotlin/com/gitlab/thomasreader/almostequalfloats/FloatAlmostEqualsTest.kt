package com.gitlab.thomasreader.almostequalfloats

import org.junit.Test

import org.junit.Assert.*
import kotlin.math.nextDown
import kotlin.math.nextUp

class FloatAlmostEqualsTest {

    @Test
    fun equalsEpsilonUlp() {
        assertEquals(true, 1f.equalsEpsilonUlp(1f, 0, 0f))
        assertEquals(true, 1f.equalsEpsilonUlp(1f.nextUp(), 1, 0f))
        assertEquals(true, 1f.equalsEpsilonUlp(1f.nextUp(), 0, Float.FLT_EPSILON))
        assertEquals(true, 0f.equalsEpsilonUlp(-0f, 0, 0f))
        assertEquals(false, 1f.equalsEpsilonUlp(1f.nextDown(), 0, 0f))
        assertEquals(false, 1f.equalsEpsilonUlp(1f.nextUp(), 0, 0f))
        assertEquals(true, 0f.nextUp().equalsEpsilonUlp(Float.FLT_EPSILON, 1, Float.FLT_EPSILON))
        assertEquals(false, 0f.nextUp().equalsEpsilonUlp(Float.FLT_EPSILON, 1, 0f))
    }

    @Test
    fun equalsEpsilonRelDiff() {
        assertEquals(true, 1f.equalsEpsilonRelDiff(1f, 0f, 0f))
        // epsilon of 0 doesn't matter as the relative difference is lte FLT_EPSILON
        assertEquals(true, 1f.equalsEpsilonRelDiff(1f.nextUp(), Float.FLT_EPSILON, 0f))
        // relDiff of 0 doesn't matter as the difference is lt FLT_EPSILON
        assertEquals(true, 1f.equalsEpsilonRelDiff(1f.nextUp(), 0f, Float.FLT_EPSILON))
        assertEquals(true, 0f.equalsEpsilonRelDiff(-0f, 0f, 0f))
        assertEquals(false, 1f.equalsEpsilonRelDiff(1f.nextDown(), 0f, 0f))
        assertEquals(false, 1f.equalsEpsilonRelDiff(1f.nextUp(), 0f, 0f))
        assertEquals(true, 0f.nextUp().equalsEpsilonRelDiff(Float.FLT_EPSILON, Float.FLT_EPSILON, Float.FLT_EPSILON))
    }

    @Test
    fun equalsEpsilon() {
        assertEquals(true, 1f.equalsEpsilon(1f, 0f))
        assertEquals(true, 1f.equalsEpsilon(1f.nextDown(), Float.FLT_EPSILON))
        assertEquals(true, 1f.equalsEpsilon(1f.nextUp(), Float.FLT_EPSILON))
        assertEquals(true, 0f.equalsEpsilon(-0f, 0f))
        assertEquals(false, 1f.equalsEpsilon(1f.nextDown(), 0f))
        assertEquals(false, 1f.equalsEpsilon(1f.nextUp(), 0f))
        assertEquals(true, 0f.nextUp().equalsEpsilon(Float.FLT_EPSILON, Float.FLT_EPSILON))
    }

    @Test
    fun equalsRelDifference() {
        // almost equal because relative difference of 1f and 1f = 0
        assertEquals(true, 1f.equalsRelDifference(1f, 0f))
        // 0f and -0f equal
        assertEquals(true, 0f.equalsRelDifference(-0f, 0f))
        // 1f and the next smallest float relative difference is lte FLT_EPSILON
        assertEquals(true, 1f.equalsRelDifference(1f.nextDown(), Float.FLT_EPSILON))
        // 1f and the next biggest float relative difference is lte FLT_EPSILON
        assertEquals(true, 1f.equalsRelDifference(1f.nextUp(), Float.FLT_EPSILON))
        // these same comparisons are now false with a relative difference of the smallest float (subnormal)
        assertEquals(false, 1f.equalsRelDifference(1f.nextDown(), Float.MIN_VALUE))
        assertEquals(false, 1f.equalsRelDifference(1f.nextUp(), Float.MIN_VALUE))
        assertEquals(false, 0f.nextUp().equalsRelDifference(Float.FLT_EPSILON, Float.FLT_EPSILON))
        assertEquals(true, 0f.nextUp().equalsRelDifference(Float.FLT_EPSILON, 1f))
    }

    @Test
    fun relativeDifference() {
        // equal so 0 difference
        assertEquals(0f, 0f.relativeDifference(-0f))
        assertEquals(0f, 1f.relativeDifference(1f))
        // order of floats makes no difference for this algorithm
        assertEquals(1f / 2f, 1f.relativeDifference(2f))
        assertEquals(1f / 2f, 2f.relativeDifference(1f))
        // involves zero and a non zero value, so expected == 1f
        assertEquals(1f, 0f.relativeDifference(1.37892E23f))
    }

    @Test
    fun equalsUlp() {
        assertEquals(true, 1f.equalsUlp(1f, 0))
        // 1f.nextDown() is 1 ulp away from 1f
        assertEquals(true, 1f.equalsUlp(1f.nextDown(), 1))
        // 1f.nextUp() is 1 ulp away from 1f
        assertEquals(true, 1f.equalsUlp(1f.nextUp(), 1))
        // -0f == 0f despite being -2147483648 ulp away
        assertEquals(true, 0f.equalsUlp(-0f, 0))
        assertEquals(false, 1f.equalsUlp(1f.nextDown(), 0))
        assertEquals(false, 1f.equalsUlp(1f.nextUp(), 0))
        // smallest subnormal and smallest normal are ~8.3+ million ulp away despite being closer in value
        // than 1f and 1.00000011920929f which are 1 ulp away
        assertEquals(false, Float.MIN_VALUE.equalsUlp(Float.fromBits(0x0080_0000), 2000))
        assertEquals(true, Float.MIN_VALUE.equalsUlp(Float.fromBits(0x0080_0000), 8388608))
    }

    @Test
    fun ulp() {
        assertEquals(0, 1f.ulp(1f))
        // this bigger than that so positive
        assertEquals(1, 1f.ulp(1f.nextDown()))
        // this smaller than that so negative
        assertEquals(-1, 1f.ulp(1f.nextUp()))
        // this in bits == 0, -0f in bits = Int.MIN_VALUE, this overflows back to Int.MIN_VALUE
        assertEquals(-2147483648, 0f.ulp(-0f))
        // this one doesn't overflow as -(Int.MIN_VALUE - 1) == Int.MAX_VALUE
        assertEquals(2147483647, 0f.ulp((-0f).nextDown()))
    }
}