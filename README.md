# Almost equal floats

Kotlin implementation for approximately comparing the equality of two floating-point numbers (float & double) in reference to some *tolerance*. This tolerance may be an epsilon, relative difference, ULP or the combination of an epsilon and a ULP or relative difference. As to which is suitable depends greatly on the nature of the floating-point numbers being used. A great source for these comparisons is a [Random ASCII blog post](https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/).

## Epsilon

The classic tolerance is a simple [machine epsilon](https://en.wikipedia.org/wiki/Machine_epsilon);
are 1.0 and 1.5 *equal* given an epsilon of 0.5:
```kotlin
println(1.0.equalsEpsilon(1.5, 0.5))
```

prints true because the absolute difference of the two numbers is 0.5 which is less than or equal to the epsilon (0.5).

The problem with an epsilon is it does not scale well in environments where the range of floating-point numbers is large. In the example above an epsilon of 0.5 seems large given 1 and 1.5, but if the numbers to be compared were 1,000,000 and 1,000,000.5 it seems like a smaller tolerance.

## Units in the last place (ULP)
Adjacent floating-point numbers have integer representations which are also adjacent; comparing these it can be calculated how far apart each representable floating-point numbers are. 

The absolute ULP of 1 and 2 are:

- Float (f32) = 8388608 = 2^23 — f32 has 23 bits of precision
- Double (f64) ~= 4.5*10^15 = 2^52 — f64 has 52 bits of precision

The smallest f32 value greater than one is `1.0000001192092896`. Getting the ULP between `1` and `1.0000001192092896` will be different between f32 and f64:

- Float (f32) = 1
- Double (f64) = 536870912 = 2^29 — as there are 29 extra bits of precision after the mantissa value of 2^-23

## Relative Difference

[Relative difference](https://en.wikipedia.org/wiki/Relative_change_and_difference#Formulae) is the absolute difference between two numbers x and y divided by some function of x and y. In this case of comparing number equality the function is the max of the absolute values of x and y. This permits that the absolute difference to never to be divided by zero because if both numbers are zero an equality check is checked before the relative difference calculation takes place.

Unlike the epsilon, relative difference scales well towards infinity in either direction but will provide a meaningless result if one of the floating-point numbers is zero and the other is not as the relative difference is **always** 1. `x =/= 0, |x-0| / max(|x|, |0|) = |x| / |x| = 1`

The relative difference between floating-point numbers is different even when the ULP difference is one. The relative difference for a given ULP `u`, integer value of the smallest floating-point mantissa `m {m ∈ ℝ | m ∈ [0, 2^precision)}` where the exponent portion of the numbers are the same:

- normal: `relative difference = u / (u + m + 2^precision)`
- sub-normal: `relative difference = u / (u + m)` ­— using this formula shows why using relative difference for very small numbers can be difficult to see how close in absolute value they are

## Combining Epsilon and ULP or Relative Difference
By combining both an epsilon and ULP or relative difference can help to cover the shortcomings of each approach: epsilons not scaling well and ULP or relative difference not performing well close to zero. 

## License

```
MIT License

Copyright (c) 2022 Tom Reader

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

```



